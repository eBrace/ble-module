#include "ioCC254x_bitdef.h"
#include "ioCC2541.h"
#include "bcomdef.h"
#include "broadcaster.h"

#define PIN_13 0x08
#define PIN_10 0x01

void button_alert();
void button_allclear();
void button_toggle();

#pragma vector = P1INT_VECTOR
__interrupt void p1_ISR(void)
{
    // Note that the order in which the following flags are cleared is important.
    // For level triggered interrupts (port interrupts) one has to clear the module
    // interrupt flag prior to clearing the CPU interrupt flags.
  
    // Clear status flag for pin with R/W0 method, see datasheet.
    P1IFG &= ~PIN_13;
    // Clear CPU interrupt status flag for P0.
    P1IF = 0;

    // call Bluetooth routine here
    P1_0 ^= 1;
    // P1_1 ^= 1;
    // button_alert();
    button_toggle();
}

void button_toggle() {
 
  uint8 flag = 0;
  GAPRole_SetParameter(GAPROLE_ADVERT_ENABLED, sizeof(flag), &flag);
  if(P1_0 == 0) {
    button_allclear();
    // button_alert();
  } else if(P1_0 == 1) {
    button_alert();
    // button_allclear();
  }
  flag = 1;
  GAPRole_SetParameter(GAPROLE_ADVERT_ENABLED, sizeof(flag), &flag);
}

void button_alert() {
  
  unsigned char data[] = {0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA};
  GAPRole_SetParameter(GAPROLE_SCAN_RSP_DATA, sizeof(data), data);
  GAPRole_SetParameter(GAPROLE_ADVERT_DATA, sizeof(data), data);
}

void button_allclear() {
  
  // unsigned char data = 0x00;
  // unsigned char data = 0xFF;
  unsigned char data[] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};
  GAPRole_SetParameter(GAPROLE_SCAN_RSP_DATA, sizeof(data), data);
  GAPRole_SetParameter(GAPROLE_ADVERT_DATA, sizeof(data), data);
}

void button_init() {
  
  // button_allclear();
  // button_toggle();
  
  P1SEL &= ~PIN_13;  // Select pin to function as General Purpose I/O.
  P1DIR &= ~PIN_13;  // Select direction as input.
  P1INP &= ~PIN_13;  // Configure as pull up/pull down.
  
  // configure LED pin
  P1SEL &= ~PIN_10;      // GPIO.
  P1DIR |= PIN_10;      // Output.
  P1_0 = 0;
  
  // Clear interrupt flags for P1.
  P1IFG &= ~PIN_13;            // Clear status flag for pin.
  P1IF = 0;                   // Clear CPU interrupt status flag for P0.
  
  // Set individual interrupt enable bit in the peripherals SFR.
  P1IEN |= PIN_13;                              // Enable interrupt from pin.
  
  // Rising edge
  // PICTL &= ~2;
  
  // Falling edge
  PICTL |= 2;
  
  // Enable P0 interrupts.
  IEN2 |= 0x10;
  // IEN2 |= 0xFF;
  
  // Enable global interrupt by setting the IEN0.EA=1.
  EA = 1;
}

void init_button_fuer_karlos() {

  P1SEL &= ~PIN_13;  // Select pin to function as General Purpose I/O.
  P1DIR &= ~PIN_13;  // Select direction as input.
  P1INP &= ~PIN_13;  // Configure as pull up/pull down.
  
  // Clear interrupt flags for P1.
  P1IFG &= ~PIN_13;            // Clear status flag for pin.
  P1IF = 0;                   // Clear CPU interrupt status flag for P0.
  
  // Set individual interrupt enable bit in the peripherals SFR.
  P1IEN |= PIN_13;                              // Enable interrupt from pin.
  
  // Rising edge
  // PICTL &= ~2;
  
  // Falling edge
  PICTL |= 2;
  
  // Enable P0 interrupts.
  IEN2 |= 0x10;
  
  // Enable global interrupt by setting the IEN0.EA=1.
  EA = 1;
}